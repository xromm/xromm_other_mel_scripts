global proc booCheckerTransHinge()

/*
PURPOSE: check Boolean mesh surface area to determine if HINGE joint pose is viable or not -- including various translations

STEPS:
--UI asks for name of Boolean mesh object, name of joint, name of translation rig cube (see Manafzadeh & Gatesy 2021 J Anat for rigging instructions), and min and max translations and number of divisions in each direction
--set min and max to same number and samples to 1 to test a single value
--creates a new Boolean-type attribute called "viable" on indicated joint
--creates an expression to query surface area of selected Boolean mesh by looping through translations and keyframe "viable"

CREATION NOTES:
last updated by AR Manafzadeh 13 February 2022
expansion of booChecker by AR Manafzadeh

CITATION INFORMAITON:
when using, please cite both Manafzadeh & Gatesy 2021 J Anat AND Manafzadeh & Gatesy 2022 ICB
*/

//***************UI CREATION************************
{
//create UI window
if (`window -exists booCTWindow` == true) deleteUI booCTWindow;  

	int $height = 400;
	int $width = 400;

string $mainWindow = `window
	-title "Check for Hinge Interpenetration (with Translations)"
	-maximizeButton false
	-minimizeButton true
	-sizeable true
	-resizeToFitChildren false
	booCTWindow `;

//make necessary float fields 
	columnLayout -adjustableColumn true;
 floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Xmin" 
	-precision 3
    	-value1  0
	xMinVal;
    floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Xmax" 
	-precision 3
    	-value1  0
	xMaxVal;
  floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "X Samples" 
	-precision 0
    	-value1  1
	XdivisionVal;
text " ";
    floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Ymin" 
	-precision 3
    	-value1  0
	yMinVal;
	floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Ymax" 
	-precision 3
    	-value1  0
	yMaxVal; 
  floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Y Samples" 
	-precision 0
    	-value1  1
	YdivisionVal;
text " ";
   floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Zmin" 
	-precision 3
    	-value1  0
	zMinVal;
    floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Zmax" 
	-precision 3
    	-value1  0
	zMaxVal;
   floatFieldGrp -numberOfFields 1
	-columnWidth3 50 40 20
    	-label "Z Samples" 
	-precision 0
    	-value1  1
	ZdivisionVal;
text " ";

//add note about how to assign single value
text -fn "boldLabelFont" -align "center" "Make Min/Max the Same and Set Samples = 1 to Test Single Value!";
text " ";

//create necessary text fields
    textFieldButtonGrp -label "Boolean Mesh" -text "Select Boolean + click Insert Selection"
	            -buttonLabel "Insert Selection" -buttonCommand booSelectButton booName;
    textFieldButtonGrp -label "Joint" -text "Select joint + click Insert Selection"
 	            -buttonLabel "Insert Selection" -buttonCommand jointSelectButton jointName;
    textFieldButtonGrp -label "Rigging Cube" -text "Select cube + click Insert Selection"
	            -buttonLabel "Insert Selection" -buttonCommand cubeSelectButton cubeName;
    text " ";

//create procedure buttons
	button -label "Check Boolean" -enable 1 -w ($width) -command checkBooTrans;
	button -label "Close" -w ($width) closeButton;
	button -edit -command "deleteUI booCTWindow" closeButton;

	showWindow $mainWindow;
	window -e -width $width -height $height $mainWindow;
}


//***********************BUTTON PROCEDURES***********************
global proc booSelectButton()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] booName;
}
global proc jointSelectButton()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] jointName;
}
global proc cubeSelectButton()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] cubeName;
}

//************************BOOLEAN CHECK PROCEDURE*****************
global proc checkBooTrans()

{
//get floats and strings from UI
	float $zminFloat[] = `floatFieldGrp -q -value zMinVal`;
	float $zmaxFloat[] = `floatFieldGrp -q -value zMaxVal`;
	float $yminFloat[] = `floatFieldGrp -q -value yMinVal`;
	float $ymaxFloat[] = `floatFieldGrp -q -value yMaxVal`;
	float $xminFloat[] = `floatFieldGrp -q -value xMinVal`;
	float $xmaxFloat[] = `floatFieldGrp -q -value xMaxVal`;

	float $zmin = $zminFloat[0];
	float $zmax = $zmaxFloat[0];
	float $ymin = $yminFloat[0];
	float $ymax = $ymaxFloat[0];
	float $xmin = $xminFloat[0];
	float $xmax = $xmaxFloat[0];

	float $xdivisionFloat[] = `floatFieldGrp -q -value XdivisionVal`;
	float $xdivisions = $xdivisionFloat[0];
	float $ydivisionFloat[] = `floatFieldGrp -q -value YdivisionVal`;
	float $ydivisions = $ydivisionFloat[0];
	float $zdivisionFloat[] = `floatFieldGrp -q -value ZdivisionVal`;
	float $zdivisions = $zdivisionFloat[0];

	string $myJoint = `textFieldButtonGrp -q -text jointName`;
	string $myBoo = `textFieldButtonGrp -q -text booName`;
	string $myCube = `textFieldButtonGrp -q -text cubeName`;


//calculate what x translations to test and create array
int $a;
float $xTrans[];
if ($xdivisions !=1)
	{
	float $xincrement = ($xmax-$xmin)/($xdivisions-1);
	for ($a=0;$a<$xdivisions;$a++)
		{
		$xTrans[$a] = $xmin + ($a*$xincrement);
		}
	}
else
	{
	$xTrans[0] = $xmin;
	}
string $xTransString = floatArrayToString($xTrans, ", ");
print ("X Translation values are: \n");
print $xTrans;

//calculate what y translations to test and create array
int $b;
float $yTrans[];
if ($ydivisions !=1)
{
	float $yincrement = ($ymax-$ymin)/($ydivisions-1);
	for ($b=0;$b<$ydivisions;$b++)
		{
		$yTrans[$b] = $ymin + ($b*$yincrement);
		}
}
else
	{
	$yTrans[0] = $ymin;
	}
string $yTransString = floatArrayToString($yTrans, ", ");
print ("Y Translation values are: \n");
print $yTrans;

//calculate what z translations to test and create array
int $c;
float $zTrans[];
if ($zdivisions !=1)
	{
	float $zincrement = ($zmax-$zmin)/($zdivisions-1);
	for ($c=0;$c<$zdivisions;$c++)
		{
		$zTrans[$c] = $zmin + ($c*$zincrement);
		}
	}
else
	{
	$zTrans[0] = $zmin;
	}
string $zTransString = floatArrayToString($zTrans, ", ");
print ("Z Translation values are: \n");
print $zTrans;

//add Boolean-type attribute called "viable" to joint
addAttr -ln "viable"  -at bool $myJoint;
setAttr -k true ($myJoint + ".viable");

//create animation expression to loop through translations, query Boolean mesh surface area, and keyframe "viable"
expression -n ($myBoo+"TransExp")
-s("float $xTransArray[] ={" +$xTransString+ "};\n"+
"float $yTransArray[] ={" +$yTransString+"};\n"+
"float $zTransArray[] ={" +$zTransString+"};\n"+
"int $viable = 0;\n"+
"setKeyframe -at viable -v $viable "+$myJoint+";\n"+
"int $i;\n"+
"int $j;\n"+
"int $k;\n"+
"int $xSize = size($xTransArray);\n"+
"int $ySize = size($yTransArray);\n"+
"int $zSize = size($zTransArray);\n"+
"int $viability = `getAttr "+$myJoint+".viable`;\n"+
"for ($i=0;$i<$xSize;$i++)\n"+
"{\n"+
"for ($j=0;$j<$ySize;$j++)\n"+
"{\n"+
"for ($k=0;$k<$zSize;$k++)\n"+
"{\n"+
"if ($viable==0)\n"+
"{\n"+
"setAttr \""+$myCube+".scaleX\" $xTransArray[$i];\n"+
"setAttr \""+$myCube+".scaleY\" $yTransArray[$j];\n"+
"setAttr \""+$myCube+".scaleZ\" $zTransArray[$k];\n"+
"float $area[] = `polyEvaluate -area "+$myBoo+"`;\n"+
"if ($area[0]>0)\n"+
"{\n"+
"$viable = 0;\n"+
"}\n"+
"else\n"+
"{\n"+		
"$viable = 1;\n"+			
"setKeyframe -at viable -v $viable "+$myJoint+";\n"+
"}\n"+
"}\n"+
"}\n"+
"}\n"+
"}\n");		
}
				