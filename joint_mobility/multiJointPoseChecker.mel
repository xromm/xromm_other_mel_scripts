global proc multiJointPoseChecker()

/*
PURPOSE: given a FK rig (digital marionette) and polygonal envelope (typically an alpha shape) representing joint mobility, create a pose viability check system with support for multiple simultaneous joints

STEPS:
-- import all polygonal envelopes of interest using the XROMM_mayaTools imp tool; NOTE: POLYGONAL ENVELOPES MUST BE CREATED SO THAT MAYA X,Y,Z = (Z rotation, Y rotation, X rotation)
-- polygonal envelopes MUST be clean for proper functionality (no non-manifold or self-intersecting faces, with uniform normals)
-- run script from the MEL command line
-- UI asks for number of simultaneous FK joints of interest: one, two, or three
-- UI asks for names of all joints and corresponding polygonal envelopes; these can be selected in the Outliner or Hypergraph Hierarchy and inserted using the UI "insert selection" button
-- shifts polygonal envelopes 500, 1000, and 1500 units in +X to displace them from the FK rig
-- creates locators representing the current pose of each joint in the rig
-- tests whether locators are inside or outside their corresponding polygonal envelopes
-- if a locator is outside (i.e., if the current rig pose is inviable based on the polygonal envelope), its envelope mesh turns RED
-- if a locator is inside (i.e., if the current rig pose is viable based on the polygonal envelope), its envelope mesh turns GREY
-- NOTE: setting Maya to two-panel view allows control of the rig in the left panel while viewing the colors of all 1-3 polygonal envelopes in the right panel

CREATION NOTES:
last updated by AR Manafzadeh 25 May 2022 for Herbst et al. Eryops work

CITATION INFORMATION:
when using, please cite Herbst et al. 2022 ICB

*/


//***********************USER INTERFACE CREATION***********************
{
if (`window -exists poseCheckerWindow` == true) deleteUI poseCheckerWindow;  

	int $height = 100;
	int $width = 500;

string $mainWindow = `window
	-title "Check Pose Viability at Multiple Joints"
	-maximizeButton false
	-minimizeButton true
	-sizeable true
	-resizeToFitChildren false
	poseCheckerWindow `;

//create radio buttons for joint number selection
columnLayout -columnWidth 500;
    rowColumnLayout -numberOfColumns 1
        -columnWidth 1 500;
	text -font "boldLabelFont" -label "Number of Joints to Test";

      radioCollection jointRadios;
	radioButton -label "One Joint" -onc numberChoiceUpdate oneButton;	radioButton -label "Two Joints" -onc numberChoiceUpdate twoButton;	radioButton -label "Three Joints" -onc numberChoiceUpdate threeButton;	
	setParent ..;	

text " ";

//create fields for joint and polygonal envelope selection
	//create elements for single joint
	
	rowColumnLayout 
		-numberOfColumns 1  
		-visible 0
		-columnWidth 1 500
		oneLayout;
	text " ";
	text -fn "boldLabelFont" -align "center" "Select Objects in Outliner/Hypergraph + Click Insert Selection";
	text " ";
	textFieldButtonGrp
		-text "Select Joint 1 of 1" 
		-buttonLabel "Insert Selection" 
		-buttonCommand oneoneJointSelect
		oneoneJointFieldGroup;
	text " ";
	textFieldButtonGrp
		-text "Select Polygonal Envelope 1 of 1" 
		-buttonLabel "Insert Selection" 
		-buttonCommand oneoneShapeSelect
		oneoneShapeFieldGroup;
	text " ";
	text -fn "boldLabelFont" -align "center" "NOTE: POLYGONAL ENVELOPES MUST BE IMPORTED SO THAT";
	text -fn "boldLabelFont" -align "center" "MAYA X, Y, Z = (Z rotation, Y rotation, X rotation)";
	text " ";

	setParent ..;	


	//create elements for two joints
	rowColumnLayout 
		-numberOfColumns 1  
		-visible 0
		-columnWidth 1 500
		twoLayout;
	text " ";
	text -fn "boldLabelFont" -align "center" "Select Objects in Outliner/Hypergraph + Click Insert Selection";
	text " ";
	textFieldButtonGrp
		-text "Select Joint 1 of 2" 
		-buttonLabel "Insert Selection" 
		-buttonCommand onetwoJointSelect
		onetwoJointFieldGroup;
	text " ";
	textFieldButtonGrp
		-text "Select Polygonal Envelope 1 of 2" 
		-buttonLabel "Insert Selection" 
		-buttonCommand onetwoShapeSelect
		onetwoShapeFieldGroup;
	text " ";
	text " ";
	textFieldButtonGrp
		-text "Select Joint 2 of 2" 
		-buttonLabel "Insert Selection" 
		-buttonCommand twotwoJointSelect
		twotwoJointFieldGroup;
	text " ";
	textFieldButtonGrp
		-text "Select Polygonal Envelope 2 of 2" 
		-buttonLabel "Insert Selection" 
		-buttonCommand twotwoShapeSelect
		twotwoShapeFieldGroup;
	text " ";
	text -fn "boldLabelFont" -align "center" "NOTE: POLYGONAL ENVELOPES MUST BE IMPORTED SO THAT";
	text -fn "boldLabelFont" -align "center" "MAYA X, Y, Z = (Z rotation, Y rotation, X rotation)";
	text " ";

	setParent ..;	

	//create elements for three joints
	rowColumnLayout
		-numberOfColumns 1  
		-visible 0
		-columnWidth 1 500
		threeLayout;
	text " ";
	text -fn "boldLabelFont" -align "center" "Select Objects in Outliner/Hypergraph + Click Insert Selection";
	text " ";
	textFieldButtonGrp
		-text "Select Joint 1 of 3" 
		-buttonLabel "Insert Selection" 
		-buttonCommand onethreeJointSelect
		onethreeJointFieldGroup;
	text " ";
	textFieldButtonGrp
		-text "Select Polygonal Envelope 1 of 3" 
		-buttonLabel "Insert Selection" 
		-buttonCommand onethreeShapeSelect
		onethreeShapeFieldGroup;
	text " ";
	text " ";
	textFieldButtonGrp
		-text "Select Joint 2 of 3" 
		-buttonLabel "Insert Selection" 
		-buttonCommand twothreeJointSelect
		twothreeJointFieldGroup;
	text " ";
	textFieldButtonGrp
		-text "Select Polygonal Envelope 2 of 3" 
		-buttonLabel "Insert Selection" 
		-buttonCommand twothreeShapeSelect
		twothreeShapeFieldGroup;
	text " ";
	text " ";
	textFieldButtonGrp
		-text "Select Joint 3 of 3" 
		-buttonLabel "Insert Selection" 
		-buttonCommand threethreeJointSelect
		threethreeJointFieldGroup;
	text " ";
	textFieldButtonGrp
		-text "Select Polygonal Envelope 3 of 3" 
		-buttonLabel "Insert Selection" 
		-buttonCommand threethreeShapeSelect
		threethreeShapeFieldGroup;
	text " ";
	text -fn "boldLabelFont" -align "center" "NOTE: POLYGONAL ENVELOPES MUST BE IMPORTED SO THAT";
	text -fn "boldLabelFont" -align "center" "MAYA X, Y, Z = (Z rotation, Y rotation, X rotation)";
	text " ";
	setParent ..;	
	
	text " ";

	//create major task buttons
	columnLayout;
	button -label "Create Pose Check System" -enable 1 -w ($width) -command checkPos;
	button -label "Close" -w ($width) closeButton;
	button -edit -command "deleteUI poseCheckerWindow" closeButton;

	showWindow $mainWindow;
	window -e -width $width -height $height $mainWindow;
}

//***************MODIFY INPUTS BASED ON CHOSEN NUMBER OF JOINTS****************
global proc numberChoiceUpdate(){	//set all object layouts to invisible
	rowColumnLayout -e -vis 0 oneLayout;
	rowColumnLayout -e -vis 0 twoLayout;
	rowColumnLayout -e -vis 0 threeLayout;

	//query button choice and switch layout accordingly	string $numberChoice = `radioCollection -q -sl jointRadios`;	
	switch ($numberChoice)
		{
		case "oneButton":		
			rowColumnLayout -e -vis 1 oneLayout;
			textFieldButtonGrp -e -text "Select Joint 1 of 1" oneoneJointFieldGroup;
			textFieldButtonGrp -e -text "Select Polygonal Envelope 1 of 1" oneoneShapeFieldGroup;
			break;
		case "twoButton":		
			rowColumnLayout -e -vis 1 twoLayout;
			textFieldButtonGrp -e -text "Select Joint 1 of 2" onetwoJointFieldGroup;
			textFieldButtonGrp -e -text "Select Polygonal Envelope 1 of 2" onetwoShapeFieldGroup;
			textFieldButtonGrp -e -text "Select Joint 2 of 2" twotwoJointFieldGroup;
			textFieldButtonGrp -e -text "Select Polygonal Envelope 2 of 2" twotwoShapeFieldGroup;
			break;
		case "threeButton":		
			rowColumnLayout -e -vis 1 threeLayout;
			textFieldButtonGrp -e -text "Select Joint 1 of 3" onethreeJointFieldGroup;
			textFieldButtonGrp -e -text "Select Polygonal Envelope 1 of 3" onethreeShapeFieldGroup;
			textFieldButtonGrp -e -text "Select Joint 2 of 3" twothreeJointFieldGroup;
			textFieldButtonGrp -e -text "Select Polygonal Envelope 2 of 3" twothreeShapeFieldGroup;
			textFieldButtonGrp -e -text "Select Joint 3 of 3" threethreeJointFieldGroup;
			textFieldButtonGrp -e -text "Select Polygonal Envelope 3 of 3" threethreeShapeFieldGroup;
			break;
		}
}

//*********************INSERT SELECTION PROCEDURES *********************
global proc oneoneJointSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] oneoneJointFieldGroup;
}

global proc oneoneShapeSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] oneoneShapeFieldGroup;
}

global proc onetwoJointSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] onetwoJointFieldGroup;
}

global proc onetwoShapeSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] onetwoShapeFieldGroup;
}

global proc onethreeJointSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] onethreeJointFieldGroup;
}

global proc onethreeShapeSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] onethreeShapeFieldGroup;
}

global proc twotwoJointSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] twotwoJointFieldGroup;
}

global proc twotwoShapeSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] twotwoShapeFieldGroup;
}

global proc twothreeJointSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] twothreeJointFieldGroup;
}

global proc twothreeShapeSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] twothreeShapeFieldGroup;
}

global proc threethreeJointSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] threethreeJointFieldGroup;
}

global proc threethreeShapeSelect()
{
	string $objs[] = `ls -sl`;
	textFieldButtonGrp -e -text $objs[0] threethreeShapeFieldGroup;
}


//*********************CREATE POSE CHECK SYSTEM****************************

global proc checkPos(){
  string $numberChoice = `radioCollection -q -sl jointRadios`;	//query button choice

  switch ($numberChoice)
	{
	case "oneButton":		
		oneCheckPos();
		break;
	case "twoButton":		
		twoCheckPos();
		break;
	case "threeButton":		
		threeCheckPos();
		break;
	}
}


//%%%%%%%%%%%%%%%%%%%%%%%%FOR ONE JOINT%%%%%%%%%%%%%%%%%%%%%%%%

global proc oneCheckPos()
{

	//load nearestPointOnMesh plugin and gather info from UI
	loadPlugin nearestPointOnMesh;
	string $oneJoint = `textFieldButtonGrp -q -text oneoneJointFieldGroup`;
	string $oneShape = `textFieldButtonGrp -q -text oneoneShapeFieldGroup`;

	//move first polygonal envelope 500 in +X
	xform -t 500 0 0 $oneShape;
	

	//create a locator representing joint pose
	string $oneJointPose = ($oneJoint + "Pose");
	spaceLocator -n $oneJointPose;
	scale 10 10 10 $oneJointPose;
	

	//move joint pose locator 500 in +X
	string $oneJointAdd500 = ($oneJoint + "Add500");
	shadingNode -asUtility plusMinusAverage -n $oneJointAdd500;
	connectAttr -f ($oneJoint+".rotateZ") ($oneJointAdd500+".input2D[0].input2Dx");
	setAttr ($oneJointAdd500+".input2D[1].input2Dx") 500;


	//connect joint rZ to pose tZ, rY to tY, rX to tZ
	connectAttr -f ($oneJoint+".rotateX") ($oneJointPose+".translateZ");
	connectAttr -f ($oneJoint+".rotateY") ($oneJointPose+".translateY");
	connectAttr -f ($oneJointAdd500+".output2D.output2Dx") ($oneJointPose+".translateX");


	//determine if pose locator is inside or outside of polygonal envelope
	string $oneJointClosestPoint = ($oneJoint + "ClosestPoint");
	createNode "nearestPointOnMesh" -n $oneJointClosestPoint;
	connectAttr -f ($oneJointPose+".translateX") ($oneJointClosestPoint+".inPositionX");
	connectAttr -f ($oneJointPose+".translateY") ($oneJointClosestPoint+".inPositionY");
	connectAttr -f ($oneJointPose+".translateZ") ($oneJointClosestPoint+".inPositionZ");
	connectAttr -f ($oneShape+"Shape.worldMesh[0]") ($oneJointClosestPoint+".inMesh");

	string $oneJointDifference = ($oneJoint + "Difference");
	shadingNode -asUtility plusMinusAverage -n $oneJointDifference;
	setAttr ($oneJointDifference+".operation") 2;
	connectAttr -f ($oneJointClosestPoint+".position") ($oneJointDifference+".input3D[0]");
	connectAttr -f ($oneJointPose+".translate") ($oneJointDifference+".input3D[1]");

	string $oneJointDot = ($oneJoint + "Dot");
	shadingNode -asUtility vectorProduct -n $oneJointDot;
	connectAttr -f ($oneJointDifference+".output3D") ($oneJointDot+".input1");
	connectAttr -f ($oneJointClosestPoint+".normal") ($oneJointDot+".input2");
	

	//color polygonal envelope grey for inside or red for outside
	string $oneJointLambert = ($oneJoint + "Lambert");
	shadingNode -asShader lambert -n $oneJointLambert;
	sets -renderable true -noSurfaceShader true -empty -name ($oneJointLambert+"SG");
	connectAttr -f ($oneJointLambert+".outColor") ($oneJointLambert+"SG.surfaceShader");
	setAttr ($oneJointLambert+".transparency") -type double3 0.5 0.5 0.5;
	sets -e -forceElement ($oneJointLambert+"SG") $oneShape;

	string $oneJointCondition = ($oneJoint + "Condition");
	shadingNode -asUtility condition -n $oneJointCondition;
	setAttr ($oneJointCondition+".operation") 3;
	connectAttr -f ($oneJointDot+".outputX") ($oneJointCondition+".firstTerm");
	setAttr ($oneJointCondition+".colorIfTrueR") .5;
	setAttr ($oneJointCondition+".colorIfTrueG") .5;
	setAttr ($oneJointCondition+".colorIfTrueB") .5;
	setAttr ($oneJointCondition+".colorIfFalseR") 1;
	setAttr ($oneJointCondition+".colorIfFalseG") 0;
	setAttr ($oneJointCondition+".colorIfFalseB") 0;
	connectAttr -f ($oneJointCondition+".outColor") ($oneJointLambert+".color");
		
}

//%%%%%%%%%%%%%%%%%%%%%%%%FOR TWO JOINTS%%%%%%%%%%%%%%%%%%%%%%%%

global proc twoCheckPos()
{

	//load nearestPointOnMesh plugin and gather info from UI
	loadPlugin nearestPointOnMesh;
	string $oneJoint = `textFieldButtonGrp -q -text onetwoJointFieldGroup`;
	string $oneShape = `textFieldButtonGrp -q -text onetwoShapeFieldGroup`;
	string $twoJoint = `textFieldButtonGrp -q -text twotwoJointFieldGroup`;
	string $twoShape = `textFieldButtonGrp -q -text twotwoShapeFieldGroup`;


	//move first polygonal envelope 500 and second polygonal envelope 1000 in +X
	xform -t 500 0 0 $oneShape;
	xform -t 1000 0 0 $twoShape;
	

	//create locators representing joint poses
	string $oneJointPose = ($oneJoint + "Pose");
	spaceLocator -n $oneJointPose;
	scale 10 10 10  $oneJointPose;
	
	string $twoJointPose = ($twoJoint + "Pose");
	spaceLocator -n $twoJointPose;
	scale 10 10 10  $twoJointPose;
	

	//move first joint pose locator 500 and second joint pose locator 1000 in +X
	string $oneJointAdd500 = ($oneJoint + "Add500");
	shadingNode -asUtility plusMinusAverage -n $oneJointAdd500;
	connectAttr -f ($oneJoint+".rotateZ") ($oneJointAdd500+".input2D[0].input2Dx");
	setAttr ($oneJointAdd500+".input2D[1].input2Dx") 500;
	
	string $twoJointAdd1000 = ($twoJoint + "Add1000");
	shadingNode -asUtility plusMinusAverage -n $twoJointAdd1000;
	connectAttr -f ($twoJoint+".rotateZ") ($twoJointAdd1000+".input2D[0].input2Dx");
	setAttr ($twoJointAdd1000+".input2D[1].input2Dx") 1000;


	//determine if pose locators are inside or outside of polygonal envelopes
	connectAttr -f ($oneJoint+".rotateX") ($oneJointPose+".translateZ");
	connectAttr -f ($oneJoint+".rotateY") ($oneJointPose+".translateY");
	connectAttr -f ($oneJointAdd500+".output2D.output2Dx") ($oneJointPose+".translateX");

	connectAttr -f ($twoJoint+".rotateX") ($twoJointPose+".translateZ");
	connectAttr -f ($twoJoint+".rotateY") ($twoJointPose+".translateY");
	connectAttr -f ($twoJointAdd1000+".output2D.output2Dx") ($twoJointPose+".translateX");

	string $oneJointClosestPoint = ($oneJoint + "ClosestPoint");
	createNode "nearestPointOnMesh" -n $oneJointClosestPoint;
	connectAttr -f ($oneJointPose+".translateX") ($oneJointClosestPoint+".inPositionX");
	connectAttr -f ($oneJointPose+".translateY") ($oneJointClosestPoint+".inPositionY");
	connectAttr -f ($oneJointPose+".translateZ") ($oneJointClosestPoint+".inPositionZ");
	connectAttr -f ($oneShape+"Shape.worldMesh[0]") ($oneJointClosestPoint+".inMesh");

	string $twoJointClosestPoint = ($twoJoint + "ClosestPoint");
	createNode "nearestPointOnMesh" -n $twoJointClosestPoint;
	connectAttr -f ($twoJointPose+".translateX") ($twoJointClosestPoint+".inPositionX");
	connectAttr -f ($twoJointPose+".translateY") ($twoJointClosestPoint+".inPositionY");
	connectAttr -f ($twoJointPose+".translateZ") ($twoJointClosestPoint+".inPositionZ");
	connectAttr -f ($twoShape+"Shape.worldMesh[0]") ($twoJointClosestPoint+".inMesh");

	string $oneJointDifference = ($oneJoint + "Difference");
	shadingNode -asUtility plusMinusAverage -n $oneJointDifference;
	setAttr ($oneJointDifference+".operation") 2;
	connectAttr -f ($oneJointClosestPoint+".position") ($oneJointDifference+".input3D[0]");
	connectAttr -f ($oneJointPose+".translate") ($oneJointDifference+".input3D[1]");

	string $twoJointDifference = ($twoJoint + "Difference");
	shadingNode -asUtility plusMinusAverage -n $twoJointDifference;
	setAttr ($twoJointDifference+".operation") 2;
	connectAttr -f ($twoJointClosestPoint+".position") ($twoJointDifference+".input3D[0]");
	connectAttr -f ($twoJointPose+".translate") ($twoJointDifference+".input3D[1]");

	string $oneJointDot = ($oneJoint + "Dot");
	shadingNode -asUtility vectorProduct -n $oneJointDot;
	connectAttr -f ($oneJointDifference+".output3D") ($oneJointDot+".input1");
	connectAttr -f ($oneJointClosestPoint+".normal") ($oneJointDot+".input2");

	string $twoJointDot = ($twoJoint + "Dot");
	shadingNode -asUtility vectorProduct -n $twoJointDot;
	connectAttr -f ($twoJointDifference+".output3D") ($twoJointDot+".input1");
	connectAttr -f ($twoJointClosestPoint+".normal") ($twoJointDot+".input2");
	

	//color polygonal envelopes grey for inside or red for outside
	string $oneJointLambert = ($oneJoint + "Lambert");
	shadingNode -asShader lambert -n $oneJointLambert;
	sets -renderable true -noSurfaceShader true -empty -name ($oneJointLambert+"SG");
	connectAttr -f ($oneJointLambert+".outColor") ($oneJointLambert+"SG.surfaceShader");
	setAttr ($oneJointLambert+".transparency") -type double3 0.5 0.5 0.5;
	sets -e -forceElement ($oneJointLambert+"SG") $oneShape;

	string $twoJointLambert = ($twoJoint + "Lambert");
	shadingNode -asShader lambert -n $twoJointLambert;
	sets -renderable true -noSurfaceShader true -empty -name ($twoJointLambert+"SG");
	connectAttr -f ($twoJointLambert+".outColor") ($twoJointLambert+"SG.surfaceShader");
	setAttr ($twoJointLambert+".transparency") -type double3 0.5 0.5 0.5;
	sets -e -forceElement ($twoJointLambert+"SG") $twoShape;

	string $oneJointCondition = ($oneJoint + "Condition");
	shadingNode -asUtility condition -n $oneJointCondition;
	setAttr ($oneJointCondition+".operation") 3;
	connectAttr -f ($oneJointDot+".outputX") ($oneJointCondition+".firstTerm");
	setAttr ($oneJointCondition+".colorIfTrueR") .5;
	setAttr ($oneJointCondition+".colorIfTrueG") .5;
	setAttr ($oneJointCondition+".colorIfTrueB") .5;
	setAttr ($oneJointCondition+".colorIfFalseR") 1;
	setAttr ($oneJointCondition+".colorIfFalseG") 0;
	setAttr ($oneJointCondition+".colorIfFalseB") 0;
	connectAttr -f ($oneJointCondition+".outColor") ($oneJointLambert+".color");

	string $twoJointCondition = ($twoJoint + "Condition");
	shadingNode -asUtility condition -n $twoJointCondition;
	setAttr ($twoJointCondition+".operation") 3;
	connectAttr -f ($twoJointDot+".outputX") ($twoJointCondition+".firstTerm");
	setAttr ($twoJointCondition+".colorIfTrueR") .5;
	setAttr ($twoJointCondition+".colorIfTrueG") .5;
	setAttr ($twoJointCondition+".colorIfTrueB") .5;
	setAttr ($twoJointCondition+".colorIfFalseR") 1;
	setAttr ($twoJointCondition+".colorIfFalseG") 0;
	setAttr ($twoJointCondition+".colorIfFalseB") 0;
	connectAttr -f ($twoJointCondition+".outColor") ($twoJointLambert+".color");
	
}


//%%%%%%%%%%%%%%%%%%%%%%%%FOR THREE JOINTS%%%%%%%%%%%%%%%%%%%%%%%%

global proc threeCheckPos()
{

	//load nearestPointOnMesh plugin and gather info from UI
	loadPlugin nearestPointOnMesh;
	string $oneJoint = `textFieldButtonGrp -q -text onethreeJointFieldGroup`;
	string $oneShape = `textFieldButtonGrp -q -text onethreeShapeFieldGroup`;
	string $twoJoint = `textFieldButtonGrp -q -text twothreeJointFieldGroup`;
	string $twoShape = `textFieldButtonGrp -q -text twothreeShapeFieldGroup`;
	string $threeJoint = `textFieldButtonGrp -q -text threethreeJointFieldGroup`;
	string $threeShape = `textFieldButtonGrp -q -text threethreeShapeFieldGroup`;

	
	//move first polygonal envelope 500, second polygonal envelope 1000, and third polygonal envelope 1500 in +X
	xform -t 500 0 0 $oneShape;
	xform -t 1000 0 0 $twoShape;
	xform -t 1500 0 0 $threeShape;
	

	//create locators representing joint pose
	string $oneJointPose = ($oneJoint + "Pose");
	spaceLocator -n $oneJointPose;
	scale 10 10 10  $oneJointPose;
	
	string $twoJointPose = ($twoJoint + "Pose");
	spaceLocator -n $twoJointPose;
	scale 10 10 10  $twoJointPose;

	string $threeJointPose = ($threeJoint + "Pose");
	spaceLocator -n $threeJointPose;
	scale 10 10 10  $threeJointPose;


	//move first joint pose locator 500, second joint pose locator 1000, and third joint pose locator 1500 in +X
	string $oneJointAdd500 = ($oneJoint + "Add500");
	shadingNode -asUtility plusMinusAverage -n $oneJointAdd500;
	connectAttr -f ($oneJoint+".rotateZ") ($oneJointAdd500+".input2D[0].input2Dx");
	setAttr ($oneJointAdd500+".input2D[1].input2Dx") 500;
	
	string $twoJointAdd1000 = ($twoJoint + "Add1000");
	shadingNode -asUtility plusMinusAverage -n $twoJointAdd1000;
	connectAttr -f ($twoJoint+".rotateZ") ($twoJointAdd1000+".input2D[0].input2Dx");
	setAttr ($twoJointAdd1000+".input2D[1].input2Dx") 1000;

	string $threeJointAdd1500 = ($threeJoint + "Add1500");
	shadingNode -asUtility plusMinusAverage -n $threeJointAdd1500;
	connectAttr -f ($threeJoint+".rotateZ") ($threeJointAdd1500+".input2D[0].input2Dx");
	setAttr ($threeJointAdd1500+".input2D[1].input2Dx") 1500;


	//determine if pose locators are inside or outside of polygonal envelopes
	connectAttr -f ($oneJoint+".rotateX") ($oneJointPose+".translateZ");
	connectAttr -f ($oneJoint+".rotateY") ($oneJointPose+".translateY");
	connectAttr -f ($oneJointAdd500+".output2D.output2Dx") ($oneJointPose+".translateX");

	connectAttr -f ($twoJoint+".rotateX") ($twoJointPose+".translateZ");
	connectAttr -f ($twoJoint+".rotateY") ($twoJointPose+".translateY");
	connectAttr -f ($twoJointAdd1000+".output2D.output2Dx") ($twoJointPose+".translateX");

	connectAttr -f ($threeJoint+".rotateX") ($threeJointPose+".translateZ");
	connectAttr -f ($threeJoint+".rotateY") ($threeJointPose+".translateY");
	connectAttr -f ($threeJointAdd1500+".output2D.output2Dx") ($threeJointPose+".translateX");

	string $oneJointClosestPoint = ($oneJoint + "ClosestPoint");
	createNode "nearestPointOnMesh" -n $oneJointClosestPoint;
	connectAttr -f ($oneJointPose+".translateX") ($oneJointClosestPoint+".inPositionX");
	connectAttr -f ($oneJointPose+".translateY") ($oneJointClosestPoint+".inPositionY");
	connectAttr -f ($oneJointPose+".translateZ") ($oneJointClosestPoint+".inPositionZ");
	connectAttr -f ($oneShape+"Shape.worldMesh[0]") ($oneJointClosestPoint+".inMesh");

	string $twoJointClosestPoint = ($twoJoint + "ClosestPoint");
	createNode "nearestPointOnMesh" -n $twoJointClosestPoint;
	connectAttr -f ($twoJointPose+".translateX") ($twoJointClosestPoint+".inPositionX");
	connectAttr -f ($twoJointPose+".translateY") ($twoJointClosestPoint+".inPositionY");
	connectAttr -f ($twoJointPose+".translateZ") ($twoJointClosestPoint+".inPositionZ");
	connectAttr -f ($twoShape+"Shape.worldMesh[0]") ($twoJointClosestPoint+".inMesh");

	string $threeJointClosestPoint = ($threeJoint + "ClosestPoint");
	createNode "nearestPointOnMesh" -n $threeJointClosestPoint;
	connectAttr -f ($threeJointPose+".translateX") ($threeJointClosestPoint+".inPositionX");
	connectAttr -f ($threeJointPose+".translateY") ($threeJointClosestPoint+".inPositionY");
	connectAttr -f ($threeJointPose+".translateZ") ($threeJointClosestPoint+".inPositionZ");
	connectAttr -f ($threeShape+"Shape.worldMesh[0]") ($threeJointClosestPoint+".inMesh");

	string $oneJointDifference = ($oneJoint + "Difference");
	shadingNode -asUtility plusMinusAverage -n $oneJointDifference;
	setAttr ($oneJointDifference+".operation") 2;
	connectAttr -f ($oneJointClosestPoint+".position") ($oneJointDifference+".input3D[0]");
	connectAttr -f ($oneJointPose+".translate") ($oneJointDifference+".input3D[1]");

	string $twoJointDifference = ($twoJoint + "Difference");
	shadingNode -asUtility plusMinusAverage -n $twoJointDifference;
	setAttr ($twoJointDifference+".operation") 2;
	connectAttr -f ($twoJointClosestPoint+".position") ($twoJointDifference+".input3D[0]");
	connectAttr -f ($twoJointPose+".translate") ($twoJointDifference+".input3D[1]");

	string $threeJointDifference = ($threeJoint + "Difference");
	shadingNode -asUtility plusMinusAverage -n $threeJointDifference;
	setAttr ($threeJointDifference+".operation") 2;
	connectAttr -f ($threeJointClosestPoint+".position") ($threeJointDifference+".input3D[0]");
	connectAttr -f ($threeJointPose+".translate") ($threeJointDifference+".input3D[1]");

	string $oneJointDot = ($oneJoint + "Dot");
	shadingNode -asUtility vectorProduct -n $oneJointDot;
	connectAttr -f ($oneJointDifference+".output3D") ($oneJointDot+".input1");
	connectAttr -f ($oneJointClosestPoint+".normal") ($oneJointDot+".input2");

	string $twoJointDot = ($twoJoint + "Dot");
	shadingNode -asUtility vectorProduct -n $twoJointDot;
	connectAttr -f ($twoJointDifference+".output3D") ($twoJointDot+".input1");
	connectAttr -f ($twoJointClosestPoint+".normal") ($twoJointDot+".input2");

	string $threeJointDot = ($threeJoint + "Dot");
	shadingNode -asUtility vectorProduct -n $threeJointDot;
	connectAttr -f ($threeJointDifference+".output3D") ($threeJointDot+".input1");
	connectAttr -f ($threeJointClosestPoint+".normal") ($threeJointDot+".input2");
	

	//color polygonal envelopes grey for inside or red for outside
	string $oneJointLambert = ($oneJoint + "Lambert");
	shadingNode -asShader lambert -n $oneJointLambert;
	sets -renderable true -noSurfaceShader true -empty -name ($oneJointLambert+"SG");
	connectAttr -f ($oneJointLambert+".outColor") ($oneJointLambert+"SG.surfaceShader");
	setAttr ($oneJointLambert+".transparency") -type double3 0.5 0.5 0.5;
	sets -e -forceElement ($oneJointLambert+"SG") $oneShape;

	string $twoJointLambert = ($twoJoint + "Lambert");
	shadingNode -asShader lambert -n $twoJointLambert;
	sets -renderable true -noSurfaceShader true -empty -name ($twoJointLambert+"SG");
	connectAttr -f ($twoJointLambert+".outColor") ($twoJointLambert+"SG.surfaceShader");
	setAttr ($twoJointLambert+".transparency") -type double3 0.5 0.5 0.5;
	sets -e -forceElement ($twoJointLambert+"SG") $twoShape;

	string $threeJointLambert = ($threeJoint + "Lambert");
	shadingNode -asShader lambert -n $threeJointLambert;
	sets -renderable true -noSurfaceShader true -empty -name ($threeJointLambert+"SG");
	connectAttr -f ($threeJointLambert+".outColor") ($threeJointLambert+"SG.surfaceShader");
	setAttr ($threeJointLambert+".transparency") -type double3 0.5 0.5 0.5;
	sets -e -forceElement ($threeJointLambert+"SG") $threeShape;

	string $oneJointCondition = ($oneJoint + "Condition");
	shadingNode -asUtility condition -n $oneJointCondition;
	setAttr ($oneJointCondition+".operation") 3;
	connectAttr -f ($oneJointDot+".outputX") ($oneJointCondition+".firstTerm");
	setAttr ($oneJointCondition+".colorIfTrueR") .5;
	setAttr ($oneJointCondition+".colorIfTrueG") .5;
	setAttr ($oneJointCondition+".colorIfTrueB") .5;
	setAttr ($oneJointCondition+".colorIfFalseR") 1;
	setAttr ($oneJointCondition+".colorIfFalseG") 0;
	setAttr ($oneJointCondition+".colorIfFalseB") 0;
	connectAttr -f ($oneJointCondition+".outColor") ($oneJointLambert+".color");

	string $twoJointCondition = ($twoJoint + "Condition");
	shadingNode -asUtility condition -n $twoJointCondition;
	setAttr ($twoJointCondition+".operation") 3;
	connectAttr -f ($twoJointDot+".outputX") ($twoJointCondition+".firstTerm");
	setAttr ($twoJointCondition+".colorIfTrueR") .5;
	setAttr ($twoJointCondition+".colorIfTrueG") .5;
	setAttr ($twoJointCondition+".colorIfTrueB") .5;
	setAttr ($twoJointCondition+".colorIfFalseR") 1;
	setAttr ($twoJointCondition+".colorIfFalseG") 0;
	setAttr ($twoJointCondition+".colorIfFalseB") 0;
	connectAttr -f ($twoJointCondition+".outColor") ($twoJointLambert+".color");

	string $threeJointCondition = ($threeJoint + "Condition");
	shadingNode -asUtility condition -n $threeJointCondition;
	setAttr ($threeJointCondition+".operation") 3;
	connectAttr -f ($threeJointDot+".outputX") ($threeJointCondition+".firstTerm");
	setAttr ($threeJointCondition+".colorIfTrueR") .5;
	setAttr ($threeJointCondition+".colorIfTrueG") .5;
	setAttr ($threeJointCondition+".colorIfTrueB") .5;
	setAttr ($threeJointCondition+".colorIfFalseR") 1;
	setAttr ($threeJointCondition+".colorIfFalseG") 0;
	setAttr ($threeJointCondition+".colorIfFalseB") 0;
	connectAttr -f ($threeJointCondition+".outColor") ($threeJointLambert+".color");

}

