global proc srjUI()

/*

PURPOSE: creates a scientific rotoscoping joint (SRJ) two-joint chain: the top joint is for positioning and orienting, the bottom joint is for animating
    
STEPS:
--UI asks for desired joint name
--names and creates joints in hierarchy: a parent joint with suffix "_pos" and a child joint with the desired names

CREATION NOTES:
last updated by AR Manafzadeh 12 February 2022
modification of srj.mel by David Baier, which lacked a UI and required a string input when running
the original srj.mel script is available in the public XROMM_mayaTools

CITATION INFORMAITON:
when using, please cite Manafzadeh & Gatesy 2022 ICB

*/


//***************UI CREATION************************
{
//create UI window
if (`window -exists srjWindow` == true) deleteUI srjWindow;  

	int $height = 100;
	int $width = 280;

string $mainWindow = `window
	-title "Create Scientific Rotoscoping Joint Chain"
	-maximizeButton false
	-minimizeButton true
	-sizeable true
	-resizeToFitChildren false
	srjWindow `;

//make necessary text field for joint name
	columnLayout;
      textFieldGrp
	-columnWidth3 50 40 20
	-label "Desired Joint Name" 
	-text "joint" 
	jointName;

text " ";
text " ";

//create procedure buttons
	button -label "Create SRJ" -enable 1 -w ($width) -command makeSRJ;
	button -label "Close" -w ($width) closeButton;
	button -edit -command "deleteUI srjWindow" closeButton;

	showWindow $mainWindow;
	window -e -width $width -height $height $mainWindow;
}


//************************SRJ CREATION PROCEDURE*****************
global proc makeSRJ()

{

//get child joint name from UI
	   string $jn = `textFieldGrp -q -text jointName`;
        select -cl;

//create parent joint name by adding suffix
        string $jpos = ($jn + "_Pos");
        
//create parent joint
        $jpos = `joint -p 0 0 0 -n $jpos`;
        joint -e -oj none $jpos;
        
//create child joint
        string $jntmp = $jn;
        int $i = 1; while (`objExists $jntmp`){$jntmp = ($jn+$i); $i++;}
        $jn = $jntmp;
        $jn = `joint -p 0 0 0 -n $jn`;
        joint -e -oj none $jn;
        setAttr ($jn + ".displayHandle") 1;
   
}
