//impGeomagicSpheres
//created 2 March 2020 by AR Manafzadeh based on original script used by Kambic et al. (2014)
//WHEN USING PLEASE CITE GATESY ET AL. (2022) JOURNAL OF ANATOMY, A PROPOSED STANDARD FOR QUANTIFYING 3-D HINDLIMB JOINT POSES IN LIVING AND EXTINCT ARCHOSAURS

//imports 1+ sphere fit(s) from Geomagic VRML 1.0 format .wrl file(s)
//creates a locator at the sphere centroid
//creates a polygonal representation of the sphere fit and colors it transparent cyan

//***********************************************************************************************


global proc impGeomagicSpheres()
{

//********************STRIP INFO FROM VRML 1.0 FILE**************************
    
//get VRML 1.0 data file
	$startPath = `workspace -q -o`;
	string $fileList[] = `fileDialog2  -dir $startPath -fm  4 -ff "(*.wrl)" -ds 1`;
	if( size($fileList) != 0) {

	for ($file in $fileList)
	{

	$fileId = fopen($file, "r");

//get VRML 1.0 file name 
	string $filename = match( "[^/\\]*$", $file );

	int $sz = size( $filename);
	if( substring( $filename, $sz, $sz ) == "\.") {
		$filename = substring( $filename, 1, ( $sz-1));
	}

//strip file extension
	string $extension = match( "[^\.]*$", $filename);
	if( size($extension) > 0) {
	int $endpos = size($filename) - size($extension) - 1;
	$filename = substring( $filename, 1, $endpos);
	}
	print ($filename + "\n");
	

//extract the 4 x 4 transformation matrix (from respective line numbers)
	string $tfm[];
	string $row[];
    
	for ($i = 1; $i < 22; $i++)
	{
        	string $line = `fgetline $fileId`;
		if( $i == 17)
			{
        		tokenizeList($line, $row);
        		appendStringArray($tfm, $row, 4);
			}
		else if($i ==  20)
			{
        		tokenizeList($line, $row);
        		appendStringArray($tfm, $row, 2);
			}
		else
			;
	}
    
	fclose $fileId;

//********************CREATE SPHERE CENTROID LOCATOR***************************

//create a locator for the sphere centroid
	spaceLocator -n $filename -p 0 0 0;

//calculate sphere centroid translations based on data from file
	float $tx = $tfm[1];
	$tx = $tx/10;
	float $ty = $tfm[2];
	$ty = $ty/10;
	float $tz = $tfm[3];
	$tz = $tz/10;

//translate the locator to the proper position
	xform -t $tx $ty $tz $filename;


//**************************CREATE SPHERE OBJ******************************

//calculate the radius of the sphere based on data from file
	float $rad = $tfm[5];
	$rad = $rad/10;

//make sure fit OBJ shader (transparent cyan) exists in scene
	if(!`objExists "fitOBJShader"`)
    	{
       		shadingNode -asShader lambert -n "fitOBJShader";
        	sets -renderable true -noSurfaceShader true -empty -name fitOBJShaderSG;
        	connectAttr -f fitOBJShader.outColor fitOBJShaderSG.surfaceShader;           
        	setAttr "fitOBJShader.color" -type double3 0 1 1;
		setAttr "fitOBJShader.transparency" -type double3 .7 .7 .7;
    	}

//create a sphere, color it, and move it to the correct position/scale it to the proper radius
	sphere -n ($filename + "Sphere");
	sets -e -forceElement fitOBJShaderSG;
	xform -t $tx $ty $tz -scale $rad $rad $rad ($filename + "Sphere");
	
	clear($tfm);
}
}
}