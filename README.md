# XROMM_other_MEL_scripts Repository

This repository contains additional XROMM and XROMM-adjacent Maya Embedded Language (MEL) scripts created by various developers. General installation instructions are available at the end of this README.

---

## Import Geomagic Primitives

* Contains scripts for importing fit geometric primitives from [Geomagic](https://www.3dsystems.com/software/geomagic-wrap) into [Maya](https://www.autodesk.com/products/maya/overview).
* [Download zip file](https://bitbucket.org/xromm/xromm_other_mel_scripts/downloads/import_geomagic_primitives_v1.zip) containing three MEL scripts for importing Geomagic fit spheres, cylinders, and planes into Maya.
	* When using these scripts, please cite [Gatesy et al. (2022)](https://onlinelibrary.wiley.com/doi/10.1111/joa.13635) A proposed standard for quantifying 3-D hindlimb joint poses in living and extinct archosaurs. Journal of Anatomy. (doi:10.1111/joa.13635)

## Joint Mobility

* Contains scripts for estimating osteological joint mobility and related utilities in [Maya](https://www.autodesk.com/products/maya/overview).
* [Download zip file](https://bitbucket.org/xromm/xromm_other_mel_scripts/downloads/joint_mobility_v1.zip) containing seven MEL scripts described by Manafzadeh & Gatesy (2022)
	* When using these scripts, please cite [Manafzadeh & Gatesy (2022)](https://academic.oup.com/icb/advance-article-abstract/doi/10.1093/icb/icac008/6548895) Advances and challenges in paleobiological reconstructions of joint mobility. Integrative Comparative Biology. (doi:10.1093/icb/icac008)
* [Download zip file](https://bitbucket.org/xromm/xromm_other_mel_scripts/downloads/multiJointPoseChecker_v1.zip) containing the multiJointPoseChecker MEL script described by Herbst et al. (2022)
	* When using this script, please cite [Herbst et al. (2022)](https://doi.org/10.1093/icb/icac083) Multi-joint analysis of pose viability supports the possibility of salamander-like hindlimb configurations in the Permian tetrapod _Eryops megacephalus_. Integrative Comparative Biology. (doi:10.1093/icb/icac083). For non-MEL scripts from this paper, please visit [this link](https://github.com/evaherbst/Alpha_graphs).

## Miscellaneous Utilities 

* Contains additional miscellaneous utility scripts.
* Click [here](https://bitbucket.org/xromm/xromm_other_mel_scripts/src/main/misc_utilities/) to view. Right-click on individual scripts and "save link as" to download.
* Required software reporting for publication of oRelFast.mel:
	* oRelFast.mel has been tested in Maya versions 2020-2025.
	* This script can be tested on any publicly available XROMM Maya scene at [xmaportal.org](xmaportal.org), such as those in the public Mastication in Miniature Pigs study.
	* Output is identical to existing outptut for the XROMM Maya Tool shelf oRel tool, fully described in its own repository, but with a run time of 1-5 seconds on a "normal" desktop computer.
	* To run the script, call it from the MEL command line and follow the instructions in the resulting dialog box to select fixed and mobile anatomical coordinate systems between which to calculate relative motion.

---

## Installation Instructions

* Close Maya before installation.
* Save the MEL scripts to the following directory, based on your operating system:
	* Windows 10: My_Documents\maya\scripts
	* Windows XP: C:\Documents and Settings\User_Account\My Documents\maya\scripts
	* Mac: Macintosh HD/Users/User_Account/Library/Preferences/Autodesk/maya/scripts
* Installation time is trivial.